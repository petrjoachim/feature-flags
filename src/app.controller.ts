import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { Response } from 'express';
import { UnleashService } from 'nestjs-unleash';


@Controller()
export class AppController {
  constructor(private readonly unleash: UnleashService) {}

  @Get()
  getHello(@Res() res: Response) {
    const result = {status: 'OK'};
    const flag = this.unleash.isEnabled('new-feature')
    if(flag) {
      result['feature'] = '1'
    };
    res.status(HttpStatus.OK).json(result); 
  }
}
