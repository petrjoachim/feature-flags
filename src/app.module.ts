import { Module } from '@nestjs/common';
import { UnleashModule } from 'nestjs-unleash';
import { AppController } from './app.controller';
import { env } from './config';
@Module({
  imports: [
    UnleashModule.forRoot({
      url: "https://gitlab.com/api/v4/feature_flags/unleash/30277030",
      instanceId: "2uyDA_7WuogwsrAxwNw7",
      appName: env,
    }),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
